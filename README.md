# memtool
memtool is a tool to access to memory of a target connected over JTAG to host.

## Requirements
The application leverage LibSocca to perform access to target using JTAG,
and on SoccaSVD, to get the list of peripherals, registers and fields.
This has only been tested with python3.6.
A SVD file that describe the target to access is required.

## Installation
memtool leverage on SoccaSVD which requires some external dependecies:
libxml2 and libxslt.
This have to be installed using the pacakge manager of your distribution:
```
sudo apt-get install libxml2-dev libxslt-dev
```

The rest of the installation use setuptools.This could installed using:
```
apt-get install python3-setuptools
```

Then, you can install pmugraph by runnig:
```
python3 setup.py install --user
```
